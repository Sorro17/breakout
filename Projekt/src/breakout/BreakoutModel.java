package breakout;

import java.util.ArrayList;
import acm.util.RandomGenerator;

/**
 * A model for the game "Breakout". Stores the game data and some constants for
 * the game.
 */
public class BreakoutModel {

	// The ball
	private Ball ball;

	// The paddle
	private Paddle paddle;

	// An array containing the bricks
	private Brick[][] bricks;

	// The current direction of the ball
	private double direction;

	// The current amount of lives the player has
	private byte lives;

	// The current amount of points the player has
	private byte points;

	// An array list containing the views
	private ArrayList<BreakoutView> views;

	/**
	 * Initializes a new model for "Breakout" with some default values.
	 */
	public BreakoutModel() {
		// The ball starts in the center of the window with size specified by
		// constants.
		ball = new Ball(WINDOW_WIDTH / 2 - BALL_SIZE / 2, WINDOW_HEIGHT / 2 - BALL_SIZE / 2, BALL_SIZE, BALL_SIZE);

		// The paddle starts at the bottom center of the screen with size
		// specified by constants.
		paddle = new Paddle(WINDOW_WIDTH / 2 - PADDLE_WIDTH / 2, WINDOW_HEIGHT - (WINDOW_HEIGHT / 10), PADDLE_WIDTH,
				PADDLE_HEIGHT);

		// Brick array is instantiated with size specified by constants.
		bricks = new Brick[BRICK_ROWS][BRICK_COLUMNS];

		assert bricks.length > 0;

		// Brick array is initialized.
		bricks = initBrickArray(bricks);

		// Ball starting direction is downwards.
		direction = 270;

		// Lives and points are set to default values and the list of views is
		// created.
		lives = 3;
		points = 0;
		views = new ArrayList<BreakoutView>();
	}

	/**
	 * Adds a view to the model and updates it.
	 * 
	 * @param view
	 *            the view to be added & updated
	 */
	public void addView(BreakoutView view) {
		views.add(view);
		notifyViews();
	}

	/**
	 * Checks whether the player has won.
	 * 
	 * @return {@code true} if the player has won, {@code false} otherwise.
	 */
	public boolean hasWon() {
		for (int i = 0; i < getBricks().length; i++) {
			for (int j = 0; j < getBricks()[i].length; j++) {
				if (getBricks()[i][j].isVisible() == true) {
					// Player hasn't won yet if a brick is still visible.
					return false;
				}
				assert getBricks()[i][j].isVisible() == false;
			}
		}

		// All bricks are destroyed.
		return true;
	}

	/**
	 * Updates all the views.
	 */
	public void notifyViews() {
		for (int i = 0; i < views.size(); i++) {
			views.get(i).update(this);
		}
	}

	/**
	 * Initializes a given array of bricks with the constant brick width and
	 * height.
	 * 
	 * @param bricks
	 *            an array of bricks to be initialized
	 * @return the initialized array
	 */
	private Brick[][] initBrickArray(Brick[][] bricks) {
		for (int i = 0; i < bricks.length; i++) {
			// Iterate through the array.
			for (int j = 0; j < bricks[i].length; j++) {
				// Calculate the x-y-coordinates for the specific brick
				double x = BRICK_GAP + j * (BRICK_WIDTH + BRICK_GAP);
				double y = BRICK_GAP + i * (BRICK_HEIGHT + BRICK_GAP);
				bricks[i][j] = new Brick(x, y, BRICK_WIDTH, BRICK_HEIGHT, RGEN.nextColor());
			}
		}
		return bricks;
	}

	////////////////////////////////////////////////////////////////////////////////////////////
	// Setter & getter

	/**
	 * Returns the ball of the current game.
	 * 
	 * @return the ball.
	 */
	public Ball getBall() {
		return ball;
	}

	/**
	 * Returns the paddle of the current game.
	 * 
	 * @return the paddle.
	 */
	public Paddle getPaddle() {
		return paddle;
	}

	/**
	 * Returns the brick array of the current game.
	 * 
	 * @return the brick array.
	 */
	public Brick[][] getBricks() {
		return bricks;
	}

	/**
	 * Returns the current direction the ball is moving in.
	 * 
	 * @return a double representing the angle of movement in degrees starting
	 *         at 0 when moving parallel to the x axis and moving up to 360
	 *         counterclockwise.
	 */
	public double getDirection() {
		return direction;
	}

	/**
	 * Sets the direction the ball is moving in to the given angle.
	 * 
	 * @param direction
	 *            a double representing the angle of movement in degrees
	 *            starting at 0 when moving parallel to the x axis and moving up
	 *            to 360 counterclockwise.
	 */
	public void setDirection(double direction) {
		this.direction = direction;
	}

	/**
	 * Returns the current amount of lives the player has left.
	 * 
	 * @return current amount of lives as a byte.
	 */
	public byte getLives() {
		assert lives >= 0;
		return lives;
	}

	/**
	 * Sets the current lives of the player to the given value and updates the
	 * label view.
	 * 
	 * @param lives
	 *            the new amount of lives as a byte.
	 */
	public void setLives(byte lives) {
		this.lives = lives;
		assert lives >= 0;
	}

	/**
	 * Returns the current amount of points the player has.
	 * 
	 * @return current amount of points as a byte.
	 */
	public byte getPoints() {
		assert points >= 0;
		return points;
	}

	/**
	 * Sets the current points of the player to the given value and updates the
	 * label view.
	 * 
	 * @param points
	 *            the new amount of points as a byte.
	 */
	public void setPoints(byte points) {
		this.points = points;
		assert points >= 0;
	}

	// Constants for the ball
	final double BALL_SIZE = 5;
	final double BALL_SPEED = 2;

	// Constants for the paddle
	final double PADDLE_WIDTH = 80;
	final double PADDLE_HEIGHT = 5;

	// Constants for scaling the window
	final int WINDOW_WIDTH = 600;
	final int WINDOW_HEIGHT = 800;

	// Constants for the bricks
	final double BRICK_GAP = 5;
	final double BRICK_WIDTH = (WINDOW_WIDTH + BRICK_GAP) / 10;
	final double BRICK_HEIGHT = WINDOW_HEIGHT / 30 - BRICK_GAP;
	final int BRICK_ROWS = (int) ((WINDOW_HEIGHT / 3) / (BRICK_HEIGHT + BRICK_GAP));
	final int BRICK_COLUMNS = (int) (WINDOW_WIDTH / (BRICK_WIDTH + BRICK_GAP));

	// A random generator for the game
	RandomGenerator RGEN = new RandomGenerator();
}