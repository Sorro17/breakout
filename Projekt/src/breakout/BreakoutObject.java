package breakout;

/**
 * Superclass for the different types of objects in the game "Breakout",
 * specifies necessary fields and provides getter and setter methods for those.
 *
 */
public class BreakoutObject {

	// x-y-coordinates of the object
	private double x;
	private double y;

	// Size of the object
	private double width;
	private double height;

	// Visibility of the object
	private boolean isVisible;
	
	/**
	 * Creates a new BreakoutObject at the given x-y-coordinates and with the
	 * given width/height.
	 * 
	 * @param x
	 *            the x-coordinate of the object.
	 * 
	 * @param y
	 *            the y-coordinate of the object.
	 * 
	 * @param width
	 *            the width of the object.
	 * 
	 * @param height
	 *            the height of the object.
	 */
	public BreakoutObject(double x, double y, double width, double height) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		this.isVisible = true;
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////
	// Setter & getter

	/**
	 * Returns the x-coordinate of the calling object.
	 * 
	 * @return the x-coordinate.
	 */
	public double getX() {
		return x;
	}

	/**
	 * Sets the x-coordinate of the calling object to the given value.
	 * 
	 * @param x
	 *            the new value for the x-coordinate.
	 */
	public void setX(double x) {
		this.x = x;
	}

	/**
	 * Returns the y-coordinate of the calling object.
	 * 
	 * @return the y-coordinate.
	 */
	public double getY() {
		return y;
	}

	/**
	 * Sets the y-coordinate of the calling object to the given value.
	 * 
	 * @param y
	 *            the new value for the y-coordinate.
	 */
	public void setY(double y) {
		this.y = y;
	}

	/**
	 * Returns the width of the calling object-
	 * 
	 * @return the width of the object.
	 */
	public double getWidth() {
		return width;
	}

	/**
	 * Sets the width of the calling object to the given value.
	 * 
	 * @param width
	 *            the new value for the width.
	 */
	public void setWidth(double width) {
		this.width = width;
	}

	/**
	 * Returns the height of the calling object-
	 * 
	 * @return the height of the object.
	 */
	public double getHeight() {
		return height;
	}

	/**
	 * Sets the height of the calling object to the given value.
	 * 
	 * @param height
	 *            the new value for the height.
	 */
	public void setHeight(double height) {
		this.height = height;
	}

	/**
	 * Checks if the calling object is visible.
	 * 
	 * @return {@code true} if the object is visible, {@code false} when not.
	 */
	public boolean isVisible() {
		return isVisible;
	}

	/**
	 * Sets the visibility of the object to the given value.
	 * 
	 * @param isVisible
	 *            the new value for the visibility, {@code true} for visible and
	 *            {@code false} for not visible.
	 */
	public void setVisible(boolean isVisible) {
		this.isVisible = isVisible;
	}
}
