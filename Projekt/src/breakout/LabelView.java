package breakout;

import java.awt.Color;
import acm.graphics.GLabel;

/**
 * The view for all the labels in the game "Breakout". Gives the visuals of the
 * lives and points stored in a given model, as well as the label telling the
 * player whether he has won or lost.
 */
public class LabelView extends BreakoutView {

	// The different labels
	private GLabel lives;
	private GLabel points;
	private GLabel endLabel;

	// Width and height of the window
	private double windowWidth;
	private double windowHeight;

	/**
	 * Creates a new label view by taking the width and height of the window and
	 * initializes their font and color.
	 * 
	 * @param windowWidth
	 *            the width of the window.
	 * @param windowHeight
	 *            the height of the window.
	 */
	public LabelView(double windowWidth, double windowHeight) {
		this.windowWidth = windowWidth;
		this.windowHeight = windowHeight;

		lives = new GLabel("");
		lives.setFont("sarif-20");
		lives.setColor(Color.BLACK);

		points = new GLabel("");
		points.setFont("sarif-20");
		points.setColor(Color.BLACK);

		endLabel = new GLabel("");
		endLabel.setFont("sarif-100");
	}

	/**
	 * Adds the labels given in the model to the label view and checks whether
	 * the end label needs to be displayed.
	 * 
	 * @param model
	 *            the model of the current game of "Breakout"
	 */
	public void update(BreakoutModel model) {
		removeAll();

		// Fetch the lives from the model and display them in the bottom left
		// corner
		lives.setLabel("Lives: " + Integer.toString(model.getLives()));
		add(lives, 3, windowHeight - 3);

		// Fetch the points from the model and display them in the bottom right
		// corner
		points.setLabel("Points: " + Integer.toString(model.getPoints()));
		add(points, windowWidth - 3 - points.getWidth(), windowHeight - 3);

		// Check the win/lose condition
		if (model.getLives() == 0 || model.hasWon()) {
			// Display the end label after determining whether the player has
			// lost or won.
			endLabel.setLabel("You " + (model.hasWon() ? "win!" : "lose!"));
			endLabel.setColor(model.hasWon() ? Color.GREEN : Color.RED);
			add(endLabel, windowWidth / 8, windowHeight / 2);
		}
	}

}
