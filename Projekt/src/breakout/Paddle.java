package breakout;

/**
 * Defines the paddle for the game "Breakout" as a {@link BreakoutObjedct} with
 * specified x-y-position, width and height.
 */
public class Paddle extends BreakoutObject {

	/**
	 * Creates a paddle at the given x-y-position with the given width,
	 * 
	 * @param x
	 *            The x-coordinate of the paddle.
	 * 
	 * @param y
	 *            The y-coordinate of the paddle.
	 * 
	 * @param width
	 *            the width of the paddle.
	 * @param height
	 *            the height of the paddle.
	 */
	public Paddle(double x, double y, double width, double height) {
		super(x, y, width, height);
	}

}
