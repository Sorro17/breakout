package breakout;

import acm.program.GraphicsProgram;

/* Detailed comment:
 * This is our implementation of "Breakout", generally speaking we applied the 
 * MCV pattern by having a BreakoutModel, BreakoutControler and BreakoutViwe class. 
 * 
 * The main program initializes a model, the two views and a controller. The controller
 * is stored as an instance variable so in the run() part of the program the controller
 * can be sent a message through its start() method.
 * 
 * An instance of the BreakoutModel class provides important game constants and stores 
 * the current state of the game inside its private fields. These are the ball and its
 * direction, the paddle and the bricks, as well as the current lives and points the
 * player has. It also provides getter/setter methods for those. The brick array is initialized
 * with a private method, which is private because it is only needed by the model and it is
 * a method of its own to make the whole code more readable. 
 * The model also saves its used views inside an array list, which is filled through 
 * the public addView(..) method. Those views are updated by the model by calling the 
 * public notifyViews() method, which is public so it can be called from the controller 
 * when the game is finished. That in turn is checked by the hasWon() method,
 * which is also public because it is needed by other classes to determine the win.
 * 
 * The BreakoutController class controls the game and its data by checking 
 * for collisions or win/lose conditions and adjusts the game data accordingly. This
 * is done by the public start() method, which will start the process of the game in which
 * its private methods do the job of checking for collisions, moving the ball and finally
 * ending the game. Those parts are broken down into methods because they are special parts
 * which would visually overflow the start() method, especially the collision check.
 * The controller also implements the mouse listener for controlling the paddle.
 * When initialized the controller takes the model to work with into a private field
 * so it can be used in all its methods.
 * 
 * The BreakoutView class provides methods to visualize the current state of the game 
 * given by a model. It is responsible for the ball, paddle and bricks. 
 * It contains a public update method, which will be called by the model, that 
 * removes the currently displayed items and adds them anew with updated position, which 
 * it gets from the model. This is done by the draw(..) method, which is overloaded with
 * the different objects (Ball, Paddle, Brick[][]) as parameters.
 * Overall the BreakoutView is a subclass of GCompound so the objects can just be added
 * to the view, which in turn will be added to the canvas in the main programs init().
 * 
 * The LabelView class is a subclass of BreakoutView, so the model can just save an
 * array-list of BreakoutView's. It stores the window width and height, as well as labels
 * for the lives, points and win/lose as private fields, so they can be used in the update
 * method, which overrides the update method from its superclass, because it needs to do
 * own/other operations.
 * 
 * We furthermore implemented a BreakoutObject class with the subclasses Ball, Paddle 
 * and Brick. Here the basic parameters of an objects in the game are defined, such as 
 * position, width/height an whether the objects is visible or not, which is needed to 
 * remove a brick when destroyed or the ball when the game is over. Also the getter and 
 * setter methods are placed here, they are all public because model, view as well as the 
 * controller all need access to this data. The Brick class also stores the color the brick 
 * will have, as well as two contains(..) methods, which are overloaded with either a GPoint
 * or the x-y-coordinates as parameters. Those methods are needed for the controller 
 * to identify a collision with a brick.
 */

/**
 * Initializes the different MCV parts of our "Breakout" implementation, as well
 * as the program window and then starts the controller.
 * 
 * @author Philipp Mielke (stu126846); Hannes Olszewski (stu126111)
 * 
 */
public class BreakoutGame extends GraphicsProgram {

	// The controller of the game as an instance variable to be used in the run
	// method.
	private BreakoutController controller;

	/**
	 * Initializes the different components (model, views, controller) and the
	 * window of the program.
	 */
	public void init() {
		// Model
		BreakoutModel model = new BreakoutModel();

		// Set window
		setSize(model.WINDOW_WIDTH, model.WINDOW_HEIGHT);
		validate();

		// Views
		BreakoutView bView = new BreakoutView();
		model.addView(bView);
		add(bView);

		LabelView lView = new LabelView(model.WINDOW_WIDTH, model.WINDOW_HEIGHT);
		model.addView(lView);
		add(lView);

		// Controller
		controller = new BreakoutController(model);
		addMouseListeners(controller);
	}

	/**
	 * Runs the Program.
	 */
	public void run() {
		controller.start();
	}
}
