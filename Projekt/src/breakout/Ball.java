package breakout;

/**
 * Defines the ball for the game "Breakout" as a {@link GOval} with specified
 * height and width, as well as default color black.
 */
public class Ball extends BreakoutObject {

	/**
	 * Constructs the ball by invoking the constructor of the {@code superclass}
	 * {@link GOval} with specified width and height and colors it black.
	 * 
	 * @param width
	 *            the width of the ball
	 * @param height
	 *            the height of the ball
	 */
	public Ball(double x, double y, double width, double height) {
		super(x, y, width, height);
	}
}
