package breakout;

import java.awt.Color;
import acm.graphics.GPoint;

/**
 * Defines a brick for the game "Breakout" as a {@link BreakoutObject} with a
 * specified x-y-coordinate, width, height and color.
 */
public class Brick extends BreakoutObject {

	private Color color;

	/**
	 * Creates a new brick at the given x-y-position with the given width,
	 * height and color.
	 * 
	 * 
	 * @param x
	 *            The x-coordinate of the brick.
	 * 
	 * @param y
	 *            The y-coordinate of the brick.
	 * 
	 * @param width
	 *            the width of the brick.
	 * @param height
	 *            the height of the brick.
	 */
	public Brick(double x, double y, double width, double height, Color color) {
		super(x, y, width, height);
		this.color = color;
	}

	/**
	 * Returns the Color of the calling brick.
	 * 
	 * @return the Color of the brick.
	 */
	public Color getColor() {
		return color;
	}

	/**
	 * Checks if the given GPoint is contained in the calling brick.
	 * 
	 * @param p
	 *            the GPoint to check.
	 * @return {@code true} if the GPoint is contained in the brick,
	 *         {@code false} otherwise.
	 */
	public boolean contains(GPoint p) {
		return contains(p.getX(), p.getY());
	}

	/**
	 * Checks if a point at the given x-y-coordinates are contained within the
	 * calling brick.
	 * 
	 * @param x
	 *            the x-coordinate to check.
	 * 
	 * @param y
	 *            the y-coordinate to check.
	 * 
	 * @return {@code true} if it is contained, {@code false} otherwise.
	 */
	public boolean contains(double x, double y) {
		return (x >= getX()) && (x <= getX() + getWidth()) && (y >= getY()) && (y <= getY() + getHeight());
	}
}
