package breakout;

import java.awt.Color;
import acm.graphics.GCompound;
import acm.graphics.GOval;
import acm.graphics.GRect;

/**
 * The view for all objects in the game "Breakout", that aren't labels (see
 * {@link LabelView} for those). Provides methods to visualize the different
 * types of BreeakoutObjects in a given model, ass well as a method to update
 * the position of those.
 */
public class BreakoutView extends GCompound {

	// Constructor
	public BreakoutView() {

	}

	/**
	 * Updates the positions of the ball, the paddle and all the bricks by
	 * removing them all from the compound and then adding them again at the new
	 * positions given in the model.
	 * 
	 * @param model
	 *            the model containing all the position and size data.
	 */
	public void update(BreakoutModel model) {
		removeAll();

		draw(model.getBall());
		draw(model.getPaddle());
		draw(model.getBricks());
	}

	/**
	 * Displays a given ball as a GOval with coordinates and size as specified
	 * by the ball and default color black.
	 * 
	 * @param b
	 *            the ball.
	 */
	private void draw(Ball b) {
		GOval ball = new GOval(b.getX(), b.getY(), b.getWidth(), b.getHeight());
		if (b.isVisible()) {
			ball.setFilled(true);
			ball.setColor(Color.BLACK);
			add(ball);
		}
	}

	/**
	 * Displays a given paddle as a GRect with coordinates and size as specified
	 * by the paddle and default color black.
	 * 
	 * @param p
	 *            the paddle.
	 */
	private void draw(Paddle p) {
		GRect paddle = new GRect(p.getX(), p.getY(), p.getWidth(), p.getHeight());
		if (p.isVisible()) {
			paddle.setFilled(true);
			paddle.setColor(Color.BLACK);
			add(paddle);
		}
	}

	/**
	 * Iterates through a 2-dimensional array of bricks and displays each brick
	 * as a GRect with coordinates, size and color as specified by the brick
	 * itself.
	 * 
	 * @param b
	 *            the 2-dimensional array of bricks.
	 */
	private void draw(Brick[][] b) {
		for (int i = 0; i < b.length; i++) {
			for (int j = 0; j < b[i].length; j++) {
				GRect currentBrick = new GRect(b[i][j].getX(), b[i][j].getY(), b[i][j].getWidth(), b[i][j].getHeight());
				if (b[i][j].isVisible()) {
					currentBrick.setFilled(true);
					currentBrick.setColor(b[i][j].getColor());
					add(currentBrick);
				}
			}
		}

	}
}
