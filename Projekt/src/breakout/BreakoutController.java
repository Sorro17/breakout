package breakout;

import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import acm.graphics.GPoint;
import acm.util.JTFTools;

/**
 * A controller for the game "Breakout". Controls the game data of the given
 * model and listens to the mouse movement.
 */
public class BreakoutController implements MouseMotionListener {

	// The model for this controller, instance variable because it's needed in
	// all methods
	private BreakoutModel model;

	/**
	 * Creates a new controller for the game "Breakout". Saves the given model
	 * for own use.
	 * 
	 * @param model
	 *            the model the controller should work with.
	 */
	public BreakoutController(BreakoutModel model) {
		this.model = model;

	}

	/**
	 * Starts the program and does the main part (movement, collision/lose/win
	 * check).
	 */
	public void start() {
		while (true) {
			// Collision check
			checkCollision();

			// Lose/win condition
			if (model.getLives() == 0 || model.hasWon()) {
				showEndScreen();
				return;
			}

			// Move the ball
			moveBall();

			JTFTools.pause(model.BALL_SPEED);
		}
	}

	/**
	 * Checks whether the ball collides at its current position either with the
	 * borders, the paddle or the bricks and changes the movement direction of
	 * the ball accordingly.
	 */
	private void checkCollision() {
		if (model.getBall().getX() <= 0 || model.getBall().getX() + model.BALL_SIZE >= model.WINDOW_WIDTH) {
			// Collision with left or right border
			if (model.getDirection() <= 180) {
				model.setDirection(180 - model.getDirection());
			} else {
				model.setDirection(180 + (360 - model.getDirection()));
			}
		} else if (model.getBall().getY() <= 0) {
			// Collision with upper border
			model.setDirection(360 - model.getDirection());
		} else if (model.getBall().getY() + model.BALL_SIZE >= model.WINDOW_HEIGHT) {
			// Collision with lower border
			model.setLives((byte) (model.getLives() - 1));
			// Spawn ball at spawning position
			model.getBall().setX(model.WINDOW_WIDTH / 2);
			model.getBall().setY(model.WINDOW_WIDTH / 2);
			model.setDirection(270);
		} else if (model.getBall().getX() >= model.getPaddle().getX()
				&& model.getBall().getX() + model.BALL_SIZE <= model.getPaddle().getX() + model.PADDLE_WIDTH
				&& model.getBall().getY() + model.BALL_SIZE >= model.getPaddle().getY()
				&& model.getBall().getY() < model.getPaddle().getY() + model.PADDLE_HEIGHT) {
			// Collision with the paddle
			model.setDirection(90 - (45 / (model.PADDLE_WIDTH / 2) * ((model.getBall().getX() + model.BALL_SIZE / 2)
					- (model.getPaddle().getX() + model.PADDLE_WIDTH / 2))));
		} else {
			GPoint topLeft = new GPoint(model.getBall().getX(), model.getBall().getY());
			GPoint topRight = new GPoint(model.getBall().getX() + model.BALL_SIZE, model.getBall().getY());
			GPoint botLeft = new GPoint(model.getBall().getX(), model.getBall().getY() + model.BALL_SIZE);
			GPoint botRight = new GPoint(model.getBall().getX() + model.BALL_SIZE,
					model.getBall().getY() + model.BALL_SIZE);

			for (int i = 0; i < model.getBricks().length; i++) {
				for (int j = 0; j < model.getBricks()[i].length; j++) {
					if (model.getBricks()[i][j].isVisible()) {
						if (model.getBricks()[i][j].contains(topLeft)) {
							// Destroy brick & set points
							model.getBricks()[i][j].setVisible(false);
							model.setPoints((byte) (model.getPoints() + 1));

							if (model.getBricks()[i][j].contains(topLeft.getX() + 1, topLeft.getY())) {
								// Like collision with upper border
								model.setDirection(360 - model.getDirection());
							} else {
								// Like collision with left border
								if (model.getDirection() <= 180) {
									model.setDirection(180 - model.getDirection());
								} else {
									model.setDirection(180 + (360 - model.getDirection()));
								}
							}
						} else if (model.getBricks()[i][j].contains(topRight)) {
							// Destroy brick & set points
							model.getBricks()[i][j].setVisible(false);
							model.setPoints((byte) (model.getPoints() + 1));

							if (model.getBricks()[i][j].contains(topRight.getX() - 1, topRight.getY())) {
								// Like collision with upper border
								model.setDirection(360 - model.getDirection());
							} else {
								// Like collision with right border
								if (model.getDirection() <= 180) {
									model.setDirection(180 - model.getDirection());
								} else {
									model.setDirection(180 + (360 - model.getDirection()));
								}
							}
						} else if (model.getBricks()[i][j].contains(botLeft)) {
							// Destroy brick & set points
							model.getBricks()[i][j].setVisible(false);
							model.setPoints((byte) (model.getPoints() + 1));

							if (model.getBricks()[i][j].contains(botLeft.getX() + 1, topLeft.getY())) {
								// Like collision with lower border
								model.setDirection(360 - model.getDirection());
							} else {
								// Like collision with left border
								if (model.getDirection() <= 180) {
									model.setDirection(180 - model.getDirection());
								} else {
									model.setDirection(180 + (360 - model.getDirection()));
								}
							}
						} else if (model.getBricks()[i][j].contains(botRight)) {
							// Destroy brick & set points
							model.getBricks()[i][j].setVisible(false);
							model.setPoints((byte) (model.getPoints() + 1));

							if (model.getBricks()[i][j].contains(botRight.getX() + 1, botRight.getY())) {
								// Like collision with lower border
								model.setDirection(360 - model.getDirection());
							} else {
								// Like collision with right border
								if (model.getDirection() <= 180) {
									model.setDirection(180 - model.getDirection());
								} else {
									model.setDirection(180 + (360 - model.getDirection()));
								}
							}
						}
					}
				}
			}
		}

		assert model.getDirection() >= 0 && model.getDirection() <= 360;
	}

	/**
	 * Shows the end screen of the game when either the player has no lives left
	 * or he has won.
	 */
	private void showEndScreen() {
		model.getBall().setVisible(false);
		model.notifyViews();
	}

	/**
	 * Moves the ball stored in the model to its new coordinates and updates the
	 * views of the model so the changes are displayed.
	 */
	private void moveBall() {
		model.getBall().setX(model.getBall().getX() + Math.cos(Math.toRadians(model.getDirection())));
		model.getBall().setY(model.getBall().getY() - Math.sin(Math.toRadians(model.getDirection())));
		model.notifyViews();
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		// Nothing to do here
	}

	/**
	 * Moves the center of the paddle to the position of the mouse when the
	 * mouse is moved.
	 */
	public void mouseMoved(MouseEvent e) {
		model.getPaddle().setX(e.getX() - model.PADDLE_WIDTH / 2);
		model.getPaddle().setY(model.getPaddle().getY());
	}
}
