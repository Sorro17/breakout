package program;

import java.awt.Color;

import MVC.*;
import acm.program.GraphicsProgram;

/**
 * This is our Breakout game for the final programming project.
 * 
 * Description of the use of decomposition, MVC, visibility and instance
 * and class variables:
 * 
 * First to the use of decomposition and MVC: MVC is a design pattern that
 * uses models, views and a controller. So we decomposed our Breakout game
 * primarily into these three objects. We have the main Breakout.java that
 * initializes the models and views and the controller. We've split the
 * ball, the player, the blocks, the hud and the powerups into models
 * containing all necessary data and views that show these data as a
 * graphical object. The controller takes all the models and the main view
 * (BreakoutView.java) and is necessary for almost the whole game. It
 * starts the model threads of the ball, the player and the powerups and
 * controls them. The methods of the game objects are inside the model. So
 * if the controller detects a collision between ball and player, it runs
 * the method of the ball that bounces it on a specific axis. If a
 * graphical object has changed position or appearance , the controller
 * will update the main view to refresh that specific object. We have
 * divided the view updates in BreakoutView into ball, player, block, HUD
 * and powerup updates to make the program faster and remove flashing from
 * fast updated objects. So the ball will be updated constantly but our
 * player only if it has been moved and the blocks only if a block was
 * destroyed. We also made a level class that contains the data of the
 * level blocks as arrays. To load the level we load all array data and
 * create the blocks. To add a new level we just have to add a new method
 * that contains the new level arrays and add it to our level array list.
 * The code inside our controller or the other scripts is divided into
 * methods to use less code and to make the code easier to understand.
 * 
 * The visibility of our variables is mostly private because we are using
 * instantiated models and views and therefore instance variables. To get
 * or change those variables outside of our instance we use getters and
 * setters. Then each instance has its own data. In the main Breakout class
 * we use partially public and static variable to use them inside our
 * controller or the models. For example the playing field width and
 * height. Because we don't instantiate our Breakout class, it's easier to
 * make these variables public instead of giving them as instances to our
 * controller and models. Because now the controller can check if our ball
 * is outside the playing field by using this public variable. Even if we
 * would instantiate our Breakout class most variables would be the same
 * because they are static. We used static variables here because we can't
 * use for example more than one playing field width.
 * 
 * @author Finn Pascal Petersen, Niklas Vieland
 */
public class Breakout extends GraphicsProgram
{
	// Init Variables to set the general properties of the Breakout game
	private static double playingFieldX = 0;
	private static double playingFieldY = 0;
	public static double playingFieldWidth = 600;
	public static double playingFieldHeight = 800;
	private static double playerWidth = 60;
	private static double playerHeight = 10;
	public static double blockWidth = 50;
	public static double blockHeight = 20;
	private static double ballDiameter = 8;
	private int lifes = 3;
	public static double ballSpeed = 7;

	/*
	 * (non-Javadoc)
	 * 
	 * @see acm.program.GraphicsProgram#init() Sets up all models and views
	 * and creates a controller.
	 */
	public void init()
	{
		setSize((int) playingFieldWidth, (int) playingFieldHeight);
		validate();

		BreakoutLevels level1 = new BreakoutLevels();
		level1.level1();
		BreakoutLevels level2 = new BreakoutLevels();
		level2.level2();

		/* Load all models and views */
		BlockModel level1Model = new BlockModel(level1.getX(), level1.getY(),
				blockWidth, blockHeight, level1.getDestroyable(),
				level1.getColor(), level1.getDestroyed(),
				level1.getHasPowerup());
		BlockModel level2Model = new BlockModel(level2.getX(), level2.getY(),
				blockWidth, blockHeight, level2.getDestroyable(),
				level2.getColor(), level2.getDestroyed(),
				level2.getHasPowerup());
		BlockModel[] nextLevel = new BlockModel[] { level1Model, level2Model };
		BallModel ballModel = new BallModel(0.5 * playingFieldWidth - 0.5
				* ballDiameter, 0.8 * playingFieldHeight - 2 * ballDiameter,
				ballDiameter, Color.black, true);
		PlayerModel playerModel = new PlayerModel(0.5 * playingFieldWidth - 0.5
				* playerWidth, 0.8 * playingFieldHeight, playerWidth,
				playerHeight, Color.CYAN, true);
		HUDModel hud = new HUDModel(lifes);
		PowerupModel powerupModel = new PowerupModel(level1.getX(),
				level1.getY(), level1.getHasPowerup());
		PowerupModel powerupModel2 = new PowerupModel(level2.getX(),
				level2.getY(), level2.getHasPowerup());
		PowerupModel[] nextPowerups = new PowerupModel[] { powerupModel,
				powerupModel2 };
		BlockView blockView = new BlockView(playingFieldWidth,
				playingFieldHeight);
		BallView ballView = new BallView(playingFieldWidth, playingFieldHeight);
		PlayerView playerView = new PlayerView(playingFieldWidth,
				playingFieldHeight);
		BreakoutView breakoutView = new BreakoutView(playingFieldWidth,
				playingFieldHeight);
		HUDView hudView = new HUDView(playingFieldWidth, playingFieldHeight);
		PowerupView powerupView = new PowerupView(playingFieldWidth,
				playingFieldHeight);

		/* Start the controller and add the graphic objects */
		BreakoutFrameController mouseController = new BreakoutFrameController(
				playingFieldWidth, playingFieldHeight); // this is the
														// mouse
														// controller
		BreakoutController controller = new BreakoutController(playerModel,
				ballModel, breakoutView, mouseController, hud, nextLevel,
				nextPowerups);

		ballModel.addController(controller);
		powerupModel.addController(controller);

		add(mouseController);
		add(breakoutView);
	}
}
