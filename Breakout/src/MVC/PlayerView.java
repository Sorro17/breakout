package MVC;

import java.awt.Color;

import acm.graphics.GRect;

/**
 * The player view to show the player as a GRect.
 */
public class PlayerView extends BreakoutView
{
	public PlayerView(double width, double height)
	{
		super(width, height);
	}

	/**
	 * Create the player graphics object.
	 * 
	 * @param player
	 *            The player model containing player data.
	 * @return The player graphics object as a GRect.
	 */
	public static GRect createPlayer(PlayerModel player)
	{
		GRect playerRect = new GRect(player.getX(), player.getY(),
				player.getWidth(), player.getHeight());
		playerRect.setFilled(player.isFilled());
		playerRect.setColor(player.getColor());
		return playerRect;
	}
}
