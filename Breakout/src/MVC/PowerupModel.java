package MVC;

import java.awt.Color;
import java.util.ArrayList;

import acm.util.RandomGenerator;
import program.Breakout;

/**
 * The powerup model containing data of powerups.
 */
public class PowerupModel implements Runnable
{

	/* Instance Variables */
	private double[] x;
	private double[] y;
	private double yDirection = 5;
	private int[] type;
	private boolean[] activated;
	BreakoutController controller;
	private RandomGenerator rgen = new RandomGenerator();
	private ArrayList<Integer> powerupLocation = new ArrayList<Integer>();

	public void run()
	{
		while (true)
		{

			if (x != null)
			{
				for (int i = 0; i < x.length; i++)
				{
					if (activated[i])
					{
						move(i);
					}
				}

				if (controller != null)
				{
					controller.updateView(5);
				}
			}

			try
			{
				Thread.sleep(15);
			} catch (InterruptedException e)
			{
				e.printStackTrace();
			}

		}
	}

	/**
	 * Create the powerups at given block locations.
	 * 
	 * @param x
	 *            The blocks x location.
	 * @param y
	 *            The blocks y location.
	 * @param hasPowerup
	 *            If the block has a powerup.
	 */
	public PowerupModel(double[] x, double[] y, boolean[] hasPowerup)
	{
		for (int i = 0; i < x.length; i++)
		{
			if (hasPowerup[i])
			{
				powerupLocation.add(i); // how many powerups are there?
			}
		}

		this.x = new double[powerupLocation.size()];
		this.y = new double[powerupLocation.size()];
		this.type = new int[powerupLocation.size()];
		this.activated = new boolean[powerupLocation.size()];

		for (int i = 0; i < powerupLocation.size(); i++)
		{
			this.x[i] = x[powerupLocation.get(i)] + 0.5 * Breakout.blockWidth;
			this.y[i] = y[powerupLocation.get(i)] + Breakout.blockHeight;
			this.type[i] = rgen.nextInt(0, 0);
			this.activated[i] = false;
		}
	}

	public void resetPowerUpLocation()
	{
		powerupLocation.clear();
	}

	public void move(int i)
	{
		y[i] += yDirection;
	}

	public void addController(BreakoutController controller)
	{
		this.controller = controller;
	}

	/* Getters and Setters */
	public double[] getX()
	{
		return x;
	}

	public void setX(double[] x)
	{
		this.x = x;
	}

	public double[] getY()
	{
		return y;
	}

	public void setY(double[] y)
	{
		this.y = y;
	}

	public int[] getType()
	{
		return type;
	}

	public void setType(int[] type)
	{
		this.type = type;
	}

	public boolean getActive(int i)
	{
		return activated[i];
	}

	public void setActive(int i)
	{
		this.activated[powerupLocation.indexOf(i)] = true;
	}

	public void deactivate(int i)
	{
		this.activated[i] = false;
	}
}
