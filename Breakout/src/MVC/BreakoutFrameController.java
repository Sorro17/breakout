package MVC;

import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;

import acm.graphics.GRect;

/**
 * The mouse controller, containing a GRect that reacts on mouse motion.
 */
public class BreakoutFrameController extends GRect implements
		MouseMotionListener
{

	private double x;

	public BreakoutFrameController(double mouseFieldWidth,
			double mouseFieldHeight)
	{
		super(0, 0, mouseFieldWidth, mouseFieldHeight);
		addMouseMotionListener(this);
	}

	public double getMouseX()
	{
		return x;
	}

	@Override
	public void mouseDragged(MouseEvent e)
	{
		// not used
	}

	@Override
	public void mouseMoved(MouseEvent e)
	{
		x = e.getX(); // write the x coordinate so it can be used from the
						// controller
	}

}
