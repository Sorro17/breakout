package MVC;

import acm.graphics.*;

/**
 * The powerup view, drawing all powerups.
 */
public class PowerupView extends BreakoutView
{

	public PowerupView(double width, double height)
	{
		super(width, height);
	}

	/**
	 * Generates all powerups as graphic objects using the powerup model
	 * data.
	 * 
	 * @param powerup
	 *            The powerup model containing the powerup data.
	 * @return All powerups as a GCompound
	 */
	public static GCompound createPowerup(PowerupModel powerup)
	{
		GCompound powerupPicture = new GCompound();

		for (int i = 0; i < powerup.getX().length; i++)
		{ // draw all active powerups
			if (powerup.getType()[i] == 0)
			{
				if (powerup.getActive(i))
				{
					GOval powerUpBG = new GOval(powerup.getX()[i] - 5,
							powerup.getY()[i], 10, 10);
					powerupPicture.add(powerUpBG);
				}
			}
		}

		return powerupPicture;
	}
}
