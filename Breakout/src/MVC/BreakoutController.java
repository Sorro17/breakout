package MVC;

import program.Breakout;

/**
 * This is the controller. It connects models and views and controls all
 * functions of this program.
 */
public class BreakoutController implements Runnable
{
	private PlayerModel player;
	private BlockModel block;
	private BallModel ball;
	private BreakoutView view;
	private BreakoutFrameController mouseController;
	private HUDModel hud;
	private PowerupModel powerUp;
	private BlockModel[] nextLevel;
	private PowerupModel[] nextPowerups;
	private int level = 1;

	private double lastPlayerX = 0;
	private boolean start = false;
	private boolean isStarted = false;
	public static boolean finished = false;

	public void run()
	{
		while (true)
		{
			// Only if started, the ball will move.
			if (start && !isStarted)
			{
				ball.setyDirection(-Breakout.ballSpeed);
				isStarted = true;
			}

			// get block data
			double[] x = block.getX();
			double[] y = block.getY();
			double width = block.getWidth();
			double height = block.getHeight();
			boolean[] destroyable = block.getDestroyable();
			boolean[] destroyed = block.getDestroyed();
			boolean[] hasPowerup = block.getHasPowerup();
			int destroyedBlocks = 0;

			for (int i = 0; i < destroyed.length; i++)
			{

				/*
				 * Counting destroyed blocks to calculate when the
				 * level/game is finished
				 */
				if (destroyed[i])
				{
					destroyedBlocks++;
				}
				if (destroyedBlocks == destroyed.length)
				{
					finished = true;
				}

				/* Collision detection with blocks */
				if (isBallInsideBlock(x[i], y[i], width, height)
						&& !destroyed[i])
				{

					// if the ball collided with a block..
					ball.bounceXAxis(); // bounce the ball
					if (destroyable[i])
					{
						destroyed[i] = true; // destroy the block if it's
												// destroyable
						updateView(3); // update the blocks
						hud.setScore(hud.getScore() + 1); // add 1 point
						updateView(4); // update the hud
						if (hasPowerup[i])
						{
							powerUp.setActive(i); // set a powerup if the
													// block
													// contains one
						}
					}

					try
					{
						Thread.sleep(20);
					} catch (InterruptedException e)
					{
						e.printStackTrace();
					}
				}
			}

			/* Checking if the level is finished */
			if (finished)
			{

				/* Checking for next level */
				if (level <= nextLevel.length)
				{
					this.block = nextLevel[level - 1];
					this.powerUp.resetPowerUpLocation();
					this.powerUp = nextPowerups[level - 1];

					level++;
					ball.setX(0.5 * Breakout.playingFieldWidth - 0.5
							* ball.getDiameter());
					ball.setY(0.8 * Breakout.playingFieldHeight - 2
							* ball.getDiameter());
					ball.bounceXAxis();
					ball.setxDirection(0);
					ball.setyDirection(0);
					isStarted = false;
					start = false;
					updateView(3);
					updateView(5);
					finished = false;
				} else
				{

					/* Displaying Winning message */
					if (!hud.isWon())
					{
						hud.setWon(true);
						updateView(4);
						ball.setxDirection(0);
						ball.setyDirection(0);
					}
				}
			}

			/* Collision detection with the player and the ceiling */
			if (isBallInsideBlock(player.getX(), player.getY(),
					player.getWidth(), player.getHeight())
					|| ball.getY() <= 0)
			{
				ball.bounceXAxis();

				if (ball.getY() > 0)
				{ // if the ball doesn't touch the ceiling, change the x
					// speed
					// of the ball if it doesn't hit the player in the
					// middle
					ball.setxDirection(((ball.getX() + 0.5 * ball.getDiameter()) - (player
							.getX() + 0.5 * player.getWidth()))
							/ (0.5 * player.getWidth()) * Breakout.ballSpeed);
				}

				try
				{
					Thread.sleep(20); // sleep to avoid the ball being
										// stuck
										// inside an object
				} catch (InterruptedException e)
				{
					e.printStackTrace();
				}
			}

			/* Collision detection with the left and right border */
			if (ball.getX() <= 0
					|| ball.getX() + ball.getDiameter() >= Breakout.playingFieldWidth)
			{
				ball.bounceYAxis(); // boune the ball

				try
				{
					Thread.sleep(20); // sleep to avoid the ball being
										// stuck
										// inside an object
				} catch (InterruptedException e)
				{
					e.printStackTrace();
				}
			}

			/* Checking lose condition */
			if (!hud.isLost())
			{
				if (ball.getY() + ball.getDiameter() >= Breakout.playingFieldHeight)
				{ // does the ball touch the bottom border?
					hud.setLife(hud.getLife() - 1); // decrease life
					updateView(4); // update hud
					if (hud.getLife() > 0)
					{ // if there's a life left, reset the ball and pause
						// the
						// game
						ball.setX(0.5 * Breakout.playingFieldWidth - 0.5
								* ball.getDiameter());
						ball.setY(0.8 * Breakout.playingFieldHeight - 2
								* ball.getDiameter());
						ball.bounceXAxis();
						ball.setxDirection(0);
						ball.setyDirection(0);
						isStarted = false;
						start = false;
					} else
					{ // Game Over!
						hud.setLost(true);
						updateView(4);
						ball.setxDirection(0);
						ball.setyDirection(0);
					}
				}

				/* getting player movement through mouse movement */
				if (mouseController.getMouseX() != lastPlayerX)
				{ // only update if the mouse has moved
					player.setX(mouseController.getMouseX() - 0.5
							* player.getWidth()); // set the player middle
													// to
													// mouse x
					updateView(2); // update player view
					lastPlayerX = mouseController.getMouseX();
					start = true;
				}

				/* Detection of picking up PowerUps */
				for (int i = 0; i < powerUp.getX().length; i++)
				{
					if (powerUp.getX()[i] >= player.getX()
							&& powerUp.getX()[i] <= player.getX()
									+ player.getWidth())
					{
						if (powerUp.getY()[i] <= player.getY()
								+ player.getHeight()
								&& powerUp.getY()[i] >= player.getY())
						{
							if (powerUp.getActive(i))
							{
								hud.setLife(hud.getLife() + 1);
								powerUp.deactivate(i);
								updateView(4);
							}
						}
					}
				}
			}
		}
	}

	/**
	 * Checks if the ball is inside a rectangle with given origin and size.
	 * 
	 * @param blockX
	 *            The x coordinate of the rectangle.
	 * @param blockY
	 *            The y coordinate of the rectangle.
	 * @param blockWidth
	 *            The width of the rectangle.
	 * @param blockHeight
	 *            The height of the rectangle.
	 * @return True if the ball is inside the given rectangle or false if
	 *         not.
	 */
	public boolean isBallInsideBlock(double blockX, double blockY,
			double blockWidth, double blockHeight)
	{
		if (ball.getX() >= blockX - ball.getDiameter()
				&& ball.getX() <= blockX + blockWidth)
		{ // check x coordinate
			if (ball.getY() <= blockY + blockHeight
					&& ball.getY() + ball.getDiameter() >= blockY)
			{ // check y coordinate
				return true; // the ball is inside this block
			}
		}
		return false; // the ball does not touch the block
	}

	/**
	 * Constructor of the controller containing the pointers to model and
	 * view instances.
	 * 
	 * @param player
	 *            The player model.
	 * @param ball
	 *            The ball model.
	 * @param view
	 *            The Breakout view.
	 * @param mouseController
	 *            The mouse control object.
	 * @param hud
	 *            The hud model.
	 * @param nextLevel
	 *            The next level array containing blockmodel.
	 * @param nextPowerups
	 *            The powerup array.
	 */
	public BreakoutController(PlayerModel player, BallModel ball,
			BreakoutView view, BreakoutFrameController mouseController,
			HUDModel hud, BlockModel[] nextLevel, PowerupModel[] nextPowerups)
	{
		// add all pointers to own instance variables
		this.player = player;
		this.block = nextLevel[0];
		this.ball = ball;
		this.view = view;
		this.mouseController = mouseController;
		this.hud = hud;
		this.powerUp = nextPowerups[0];
		this.nextLevel = nextLevel;
		this.nextPowerups = nextPowerups;

		/* update all views at the first start */
		updateView(1);
		updateView(2);
		updateView(3);
		updateView(4);

		/* Start threads */
		Thread move = new Thread(ball, "move"); // the ball thread
		move.start();

		Thread collision = new Thread(this, "collision"); // the collision
															// detection
															// inside
															// this
															// controller
		collision.start();

		Thread controlPowerup = new Thread(powerUp, "movePowerup"); // the
																	// powerup
																	// thread
		controlPowerup.start();
	}

	/**
	 * Updates the chosen view.
	 * 
	 * @param update
	 *            1 for ballview update. 2 for the player update. 3 for
	 *            block update. 4 for the HUD update. 5 for powerup update
	 */
	public void updateView(int update)
	{
		switch (update)
		{
		case 1:
			view.updateBall(ball);
			break;
		case 2:
			view.updatePlayer(player);
			break;
		case 3:
			view.updateBlocks(block);
			break;
		case 4:
			view.updateHUD(hud);
			break;
		case 5:
			view.updatePowerup(powerUp);
			break;
		}
	}
}
