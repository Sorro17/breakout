package MVC;

import acm.graphics.GOval;

/**
 * This is the view for the ball. It can generate the ball as a graphics
 * object.
 */
public class BallView extends BreakoutView
{
	public BallView(double width, double height)
	{
		super(width, height);
	}

	/**
	 * Creates a ball with GOval using the ballmodels data.
	 * 
	 * @param ball
	 *            The ballmodel which contains the data.
	 * @return The graphics object GOval
	 */
	public static GOval createBall(BallModel ball)
	{
		GOval newBall = new GOval(ball.getX(), ball.getY(), ball.getDiameter(),
				ball.getDiameter());
		newBall.setFilled(ball.isFilled());
		newBall.setColor(ball.getColor());
		return newBall;
	}
}
