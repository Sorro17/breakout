package MVC;

import acm.graphics.*;
import program.Breakout;

/**
 * The HUD view.
 */
public class HUDView extends BreakoutView
{
	public HUDView(double width, double height)
	{
		super(width, height);
	}

	/**
	 * Generates the graphical HUD object using a GCompound.
	 * 
	 * @param hud
	 *            The HUD model containing the HUD data.
	 * @return The HUD as a GCompound
	 */
	public static GCompound createHUD(HUDModel hud)
	{
		GCompound hudCompound = new GCompound(); // Create a new GCompound

		if (hud.isLost())
		{ // Draw "Game over!" if the game is lost.
			GLabel gameOver = new GLabel("Game Over!",
					0.5 * Breakout.playingFieldWidth - 20,
					0.5 * Breakout.playingFieldHeight - 50);
			GLabel lost = new GLabel("You got to level " + hud.getLevel()
					+ " and your score is " + hud.getScore() + ".",
					0.5 * Breakout.playingFieldWidth - 100,
					0.5 * Breakout.playingFieldHeight);
			hudCompound.add(gameOver);
			hudCompound.add(lost);
		} else
		{
			// Else draw the score..
			GLabel score = new GLabel("Score: " + hud.getScore(),
					Breakout.playingFieldWidth - 60,
					Breakout.playingFieldHeight - 20);
			hudCompound.add(score);

			// ..the lifes..
			GLabel lifes = new GLabel("Lifes", 20,
					Breakout.playingFieldHeight - 20);
			hudCompound.add(lifes);
			for (int i = 0; i < hud.getLife(); i++)
			{
				GOval newLife = new GOval(30, Breakout.playingFieldHeight - 40
						- 10 * i, 5, 5);
				newLife.setFilled(true);
				hudCompound.add(newLife);
			}

			// ..and if won, the winner label.
			if (hud.isWon())
			{
				GLabel gameWon = new GLabel("You won!",
						0.5 * Breakout.playingFieldWidth - 20,
						0.5 * Breakout.playingFieldHeight - 50);
				GLabel won = new GLabel(
						"You cleared all levels and your score is "
								+ hud.getScore() + ".",
						0.5 * Breakout.playingFieldWidth - 100,
						0.5 * Breakout.playingFieldHeight);
				hudCompound.add(gameWon);
				hudCompound.add(won);
			}
		}

		return hudCompound;
	}
}
