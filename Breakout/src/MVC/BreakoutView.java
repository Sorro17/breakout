package MVC;

import java.awt.Color;

import acm.graphics.*;

/**
 * The general view that can update every view.
 */
public class BreakoutView extends GCompound
{
	private GRect lastPlayer;
	private GOval lastBall;
	private GRect[] lastBlock;
	private GCompound lastHUD;
	private GCompound lastPowerup;

	public BreakoutView(double width, double height)
	{
		background = new GRect(width, height);
		background.setFilled(true);
		background.setColor(Color.WHITE);
	}

	/*
	 * All methods are working after the same principle by checking if
	 * there already was an object and if so it gets removed and a new
	 * refreshed object will be added
	 */

	/**
	 * Updates the ball view with the given Ball Model data.
	 * 
	 * @param ball
	 *            The ball model.
	 */
	public void updateBall(BallModel ball)
	{
		if (lastBall != null)
		{
			remove(lastBall);
		}
		lastBall = BallView.createBall(ball);
		add(lastBall);
	}

	/**
	 * Updates the player view with the given player model data.
	 * 
	 * @param player
	 *            The player model.
	 */
	public void updatePlayer(PlayerModel player)
	{
		if (lastPlayer != null)
		{
			remove(lastPlayer);
		}

		lastPlayer = PlayerView.createPlayer(player);
		add(lastPlayer);
	}

	/**
	 * Updates the block view with the given block model data.
	 * 
	 * @param block
	 *            The block model.
	 */
	public void updateBlocks(BlockModel block)
	{
		if (lastBlock == null || BreakoutController.finished)
		{
			lastBlock = new GRect[block.getX().length];
		}

		for (int i = 0; i < block.getX().length; i++)
		{
			if (lastBlock[i] != null)
			{
				remove(lastBlock[i]);
			}
			lastBlock[i] = BlockView.createBlock(block)[i];
			add(lastBlock[i]);
		}
	}

	/**
	 * Updates the hud view with the given hud model data.
	 * 
	 * @param hud
	 *            The hud model.
	 */
	public void updateHUD(HUDModel hud)
	{
		if (lastHUD != null)
		{
			remove(lastHUD);
		}
		lastHUD = HUDView.createHUD(hud);
		add(lastHUD);
	}

	/**
	 * Updates the powerUp view with the given powerUp model data.
	 * 
	 * @param hud
	 *            The powerUp model.
	 */
	public void updatePowerup(PowerupModel powerUp)
	{
		if (lastPowerup != null)
		{
			remove(lastPowerup);
		}
		lastPowerup = PowerupView.createPowerup(powerUp);
		add(lastPowerup);
	}

	private GRect background;
}
