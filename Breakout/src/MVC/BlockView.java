package MVC;

import java.awt.Color;

import acm.graphics.GRect;

/**
 * This is the view for the blocks. It generates an array of GRects using
 * the blockmodels data.
 */
public class BlockView extends BreakoutView
{
	public BlockView(double width, double height)
	{
		super(width, height);
	}

	/**
	 * Generates an array of GRects from the given blockmodel data.
	 * 
	 * @param block
	 *            The blockmodel that contains the data.
	 * @return All GRects as an array.
	 */
	public static GRect[] createBlock(BlockModel block)
	{
		/* Read in all data */
		double[] x = block.getX();
		double[] y = block.getY();
		double width = block.getWidth();
		double height = block.getHeight();
		Color[] color = block.getColor();
		boolean[] destroyed = block.getDestroyed();

		GRect[] blocks = new GRect[x.length]; // initialize GRect array

		/* Create all blocks as GRects */
		for (int i = 0; i < x.length; i++)
		{
			GRect currentBlock = new GRect(0, 0);
			if (!destroyed[i])
			{
				currentBlock.setLocation(x[i], y[i]);
				currentBlock.setSize(width, height);
				currentBlock.setColor(Color.WHITE);
				currentBlock.setFilled(true);
				currentBlock.setFillColor(color[i]);
			}

			blocks[i] = currentBlock;
		}
		return blocks;
	}
}
