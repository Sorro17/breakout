package MVC;

import java.awt.Color;

import acm.graphics.GCompound;
import acm.graphics.GObject;
import acm.graphics.GRect;

/**
 * This is the Block model. It contains data of all blocks using arrays.
 */
public class BlockModel
{
	/* Instance Variables */
	private boolean[] destroyable;
	private double[] x;
	private double[] y;
	private double width;
	private double height;
	private Color[] color;
	private boolean[] destroyed;
	private boolean[] hasPowerup;

	/**
	 * Constructs a fully customizable block
	 * 
	 * @param x
	 *            Position on the X-Axis
	 * @param y
	 *            Position on the Y-Axis
	 * @param width
	 *            Block width
	 * @param height
	 *            Block height
	 * @param destroyable
	 *            Set whether is breakable or not
	 * @param color
	 *            Set the color of the block
	 * @param filled
	 *            Set whether the block is filled or not
	 */
	public BlockModel(double[] x, double[] y, double width, double height,
			boolean[] destroyable, Color[] color, boolean[] destroyed,
			boolean[] hasPowerup)
	{
		/* initialize all arrays with correct length */
		this.x = new double[x.length];
		this.y = new double[y.length];
		this.width = width;
		this.height = height;
		this.destroyable = new boolean[destroyable.length];
		this.color = new Color[color.length];
		this.destroyed = new boolean[destroyed.length];
		this.hasPowerup = new boolean[hasPowerup.length];

		/*
		 * Overwrite array data so we not use the level arrays to not
		 * overwrite them later
		 */
		for (int i = 0; i < x.length; i++)
		{
			this.x[i] = x[i];
			this.y[i] = y[i];
			this.destroyable[i] = destroyable[i];
			this.color[i] = color[i];
			this.destroyed[i] = destroyed[i];
			this.hasPowerup[i] = hasPowerup[i];
		}
	}

	/* Getters and Setters */

	public boolean[] getDestroyable()
	{
		return destroyable;
	}

	public void setDestroyable(boolean[] destroyable)
	{
		this.destroyable = destroyable;
	}

	public double[] getX()
	{
		return x;
	}

	public void setX(double[] x)
	{
		this.x = x;
	}

	public double[] getY()
	{
		return y;
	}

	public void setY(double[] y)
	{
		this.y = y;
	}

	public double getWidth()
	{
		return width;
	}

	public void setWidth(double width)
	{
		this.width = width;
	}

	public double getHeight()
	{
		return height;
	}

	public void setHeight(double height)
	{
		this.height = height;
	}

	public Color[] getColor()
	{
		return color;
	}

	public void setColor(Color[] color)
	{
		this.color = color;
	}

	public boolean[] getDestroyed()
	{
		return destroyed;
	}

	public void setDestroyed(boolean[] filled)
	{
		this.destroyed = destroyed;
	}

	public boolean[] getHasPowerup()
	{
		return hasPowerup;
	}

	public void setHasPowerup(boolean[] hasPowerup)
	{
		this.hasPowerup = hasPowerup;
	}
}
