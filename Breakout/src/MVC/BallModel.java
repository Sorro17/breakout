package MVC;

import java.awt.Color;

import acm.graphics.GOval;

/**
 * This is the ball model. It contains the balls data and methods.
 */
public class BallModel implements Runnable
{
	/* Instance Variables */
	private double xDirection = 0;
	private double yDirection = 0;
	private double x;
	private double y;
	private double diameter;
	private Color color;
	private boolean filled;
	BreakoutController controller;

	public void run()
	{
		while (true)
		{
			move(); // move the ball constantly

			if (controller != null)
			{
				controller.updateView(1); // update the ballview through
											// the controller
			}

			try
			{
				Thread.sleep(15);
			} catch (InterruptedException e)
			{
				e.printStackTrace();
			}
		}
	}

	/**
	 * Creates a ball with customizable diameter at any given point in any
	 * given color either outlined or filled
	 * 
	 * @param x
	 *            Position on the X-Axis
	 * @param y
	 *            Position on the Y-Axis
	 * @param diameter
	 *            Sets the diameter of the ball
	 * @param color
	 *            Sets the color of the ball
	 * @param fillable
	 *            Sets whether the ball is filled or not
	 */
	public BallModel(double x, double y, double diameter, Color color,
			boolean filled)
	{
		this.x = x;
		this.y = y;
		this.diameter = diameter;
		this.color = color;
		this.filled = filled;
	}

	/**
	 * Add a pointer to the controller to use the controllers methods.
	 * 
	 * @param controller
	 *            The used controller.
	 */
	public void addController(BreakoutController controller)
	{
		this.controller = controller;
	}

	/* Methods */

	/**
	 * Bounces the ball at a vertical axis, inverting the balls x speed.
	 */
	public void bounceYAxis()
	{
		this.setxDirection(-xDirection);
	}

	/**
	 * Bounces the ball at a horizontal axis, inverting the balls y speed.
	 */
	public void bounceXAxis()
	{
		this.setyDirection(-yDirection);
	}

	/**
	 * Moves the ball the amount of xDirection and yDirection. Continuous
	 * updating for a move animation.
	 */
	public void move()
	{
		x += xDirection;
		y += yDirection;
	}

	/* Getters and Setters */

	public double getX()
	{
		return x;
	}

	public void setX(double x)
	{
		this.x = x;
	}

	public double getY()
	{
		return y;
	}

	public void setY(double y)
	{
		this.y = y;
	}

	public double getDiameter()
	{
		return diameter;
	}

	public void setDiameter(double diameter)
	{
		this.diameter = diameter;
	}

	public Color getColor()
	{
		return color;
	}

	public void setColor(Color color)
	{
		this.color = color;
	}

	public boolean isFilled()
	{
		return filled;
	}

	public void setFilled(boolean filled)
	{
		this.filled = filled;
	}

	public double getxDirection()
	{
		return xDirection;
	}

	public void setxDirection(double xDirection)
	{
		this.xDirection = xDirection;
	}

	public double getyDirection()
	{
		return yDirection;
	}

	public void setyDirection(double yDirection)
	{
		this.yDirection = yDirection;
	}
}
