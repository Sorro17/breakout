package MVC;

/**
 * This is the HUD model. It contains the data for the HUD, for example the
 * lifes and the score of the player.
 */
public class HUDModel
{

	/* The data for the HUD */
	private int lifes;
	private int score = 0;
	private boolean lost = false;
	private boolean won = false;
	private int level = 1;

	/* Getters and Setters */
	public int getLevel()
	{
		return level;
	}

	public void setLevel(int level)
	{
		this.level = level;
	}

	public boolean isLost()
	{
		return lost;
	}

	public void setLost(boolean lost)
	{
		this.lost = lost;
	}

	public boolean isWon()
	{
		return won;
	}

	public void setWon(boolean won)
	{
		this.won = won;
	}

	public HUDModel(int lifes)
	{
		this.lifes = lifes;
	}

	public void setLife(int newLife)
	{
		lifes = newLife;
	}

	public int getLife()
	{
		return lifes;
	}

	public int getScore()
	{
		return score;
	}

	public void setScore(int score)
	{
		this.score = score;
	}
}
