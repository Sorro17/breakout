package MVC;

import java.awt.Color;

import java.awt.Color;

/**
 * The player model containing player data and player methods.
 */
public class PlayerModel
{
	/* Instance Variables */
	private double speed = 1;
	private boolean moving = false;
	private boolean filled;
	private double x = 0;
	private double y = 0;
	private double width = 0;
	private double height = 0;
	private Color color;

	/**
	 * Create the player with given parameters.
	 * 
	 * @param x
	 *            The x position of the player.
	 * @param y
	 *            The y position of the player.
	 * @param width
	 *            The player width.
	 * @param height
	 *            The player height.
	 * @param color
	 *            The player color.
	 * @param filled
	 *            Is the graphic object filled?
	 */
	public PlayerModel(double x, double y, double width, double height,
			Color color, boolean filled)
	{
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		this.color = color;
		this.filled = filled;
	}

	/* Getters and Setters */
	public boolean isFilled()
	{
		return filled;
	}

	public void setFilled(boolean filled)
	{
		this.filled = filled;
	}

	public double getX()
	{
		return x;
	}

	public void setX(double x)
	{
		this.x = x;
	}

	public double getY()
	{
		return y;
	}

	public void setY(double y)
	{
		this.y = y;
	}

	public double getWidth()
	{
		return width;
	}

	public void setWidth(double width)
	{
		this.width = width;
	}

	public double getHeight()
	{
		return height;
	}

	public void setHeight(double height)
	{
		this.height = height;
	}

	public Color getColor()
	{
		return color;
	}

	public void setColor(Color color)
	{
		this.color = color;
	}

	public double getSpeed()
	{
		return speed;
	}

	public void setSpeed(double speed)
	{
		this.speed = speed;
	}

	public boolean isMoving()
	{
		return moving;
	}

	public void setMoving(boolean moving)
	{
		this.moving = moving;
	}

	public void moveLeft()
	{
		x = x - speed;
	}

	public void moveRight()
	{
		x = x + speed;
	}
}
