package MVC;

import java.awt.Color;

/**
 * The Breakout levels. The levels contains arrays that describe the data
 * of all blocks.
 */
public class BreakoutLevels
{
	private double[] x;
	private double[] y;
	private boolean[] destroyable;
	private Color[] color;
	private boolean[] destroyed;
	private boolean[] hasPowerup;

	/**
	 * The first level.
	 */
	public void level1()
	{
		double[] x = { 0, 50, 100, 150, 200, 250, 300, 350, 400, 450, 500, 550,
				0, 50, 100, 150, 200, 250, 300, 350, 400, 450, 500, 550 };

		double[] y = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 20, 20, 20, 20, 20,
				20, 20, 20, 20, 20, 20, 20 };

		boolean[] destroyable = { true, true, true, true, true, true, true,
				true, true, true, true, true, true, true, true, true, true,
				true, true, true, true, true, true, true };

		Color[] color = { Color.RED, Color.RED, Color.RED, Color.RED,
				Color.RED, Color.RED, Color.RED, Color.RED, Color.RED,
				Color.RED, Color.RED, Color.RED, Color.RED, Color.RED,
				Color.RED, Color.RED, Color.RED, Color.RED, Color.RED,
				Color.RED, Color.RED, Color.RED, Color.RED, Color.RED };

		boolean[] destroyed = { false, false, false, false, false, false,
				false, false, false, false, false, false, false, false, false,
				false, false, false, false, false, false, false, false, false };

		boolean[] hasPowerup = { false, false, false, false, false, true,
				false, false, false, false, false, false, false, false, true,
				false, false, false, false, false, false, false, false, false };

		/* Load the level into instance variables */
		this.x = x;
		this.y = y;
		this.color = color;
		this.destroyable = destroyable;
		this.destroyed = destroyed;
		this.hasPowerup = hasPowerup;
	}

	/**
	 * The second level.
	 */
	public void level2()
	{
		double[] x = { 0, 50, 100, 150, 200, 250, 300, 350, 400, 450, 500, 550,
				0, 50, 100, 150, 200, 250, 300, 350, 400, 450, 500, 550, 0, 50,
				100, 150, 200, 250, 300, 350, 400, 450, 500, 550, 0, 50, 100,
				150, 200, 250, 300, 350, 400, 450, 500, 550, 0, 50, 100, 150,
				200, 250, 300, 350, 400, 450, 500, 550, 0, 50, 100, 150, 200,
				250, 300, 350, 400, 450, 500, 550 };

		double[] y = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 20, 20, 20, 20, 20,
				20, 20, 20, 20, 20, 20, 20, 40, 40, 40, 40, 40, 40, 40, 40, 40,
				40, 40, 40, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 80,
				80, 80, 80, 80, 80, 80, 80, 80, 80, 80, 80, 100, 100, 100, 100,
				100, 100, 100, 100, 100, 100, 100, 100 };

		boolean[] destroyable = { true, true, true, true, true, true, true,
				true, true, true, true, true, true, true, true, true, true,
				true, true, true, true, true, true, true, true, true, true,
				true, true, true, true, true, true, true, true, true, true,
				true, true, true, true, true, true, true, true, true, true,
				true, true, true, true, true, true, true, true, true, true,
				true, true, true, true, true, true, true, true, true, true,
				true, true, true, true, true };

		Color[] color = { Color.RED, Color.RED, Color.RED, Color.RED,
				Color.RED, Color.RED, Color.RED, Color.RED, Color.RED,
				Color.RED, Color.RED, Color.RED, Color.RED, Color.RED,
				Color.RED, Color.RED, Color.RED, Color.RED, Color.RED,
				Color.RED, Color.RED, Color.RED, Color.RED, Color.RED,
				Color.RED, Color.RED, Color.RED, Color.RED, Color.RED,
				Color.RED, Color.RED, Color.RED, Color.RED, Color.RED,
				Color.RED, Color.RED, Color.RED, Color.RED, Color.RED,
				Color.RED, Color.RED, Color.RED, Color.RED, Color.RED,
				Color.RED, Color.RED, Color.RED, Color.RED, Color.RED,
				Color.RED, Color.RED, Color.RED, Color.RED, Color.RED,
				Color.RED, Color.RED, Color.RED, Color.RED, Color.RED,
				Color.RED, Color.RED, Color.RED, Color.RED, Color.RED,
				Color.RED, Color.RED, Color.RED, Color.RED, Color.RED,
				Color.RED, Color.RED, Color.RED };

		boolean[] destroyed = { false, false, false, false, false, false,
				false, false, false, false, false, false, false, false, false,
				false, false, false, false, false, false, false, false, false,
				false, false, false, false, false, false, false, false, false,
				false, false, false, false, false, false, false, false, false,
				false, false, false, false, false, false, false, false, false,
				false, false, false, false, false, false, false, false, false,
				false, false, false, false, false, false, false, false, false,
				false, false, false };

		boolean[] hasPowerup = { false, false, false, false, false, false,
				false, false, false, false, false, false, true, false, false,
				false, false, false, false, false, true, false, false, false,
				false, false, true, false, false, false, false, false, false,
				false, false, false, false, false, false, false, false, false,
				false, true, false, false, false, false, false, false, false,
				false, false, false, false, false, false, false, false, false,
				false, false, false, false, false, false, false, false, false,
				false, false, true };

		/* Load the level into instance variables */
		this.x = x;
		this.y = y;
		this.color = color;
		this.destroyable = destroyable;
		this.destroyed = destroyed;
		this.hasPowerup = hasPowerup;
	}

	/* Getters and Setters */

	public boolean[] getHasPowerup()
	{
		return hasPowerup;
	}

	public void setHasPowerup(boolean[] hasPowerup)
	{
		this.hasPowerup = hasPowerup;
	}

	public double[] getX()
	{
		return x;
	}

	public void setX(double[] x)
	{
		this.x = x;
	}

	public double[] getY()
	{
		return y;
	}

	public void setY(double[] y)
	{
		this.y = y;
	}

	public boolean[] getDestroyable()
	{
		return destroyable;
	}

	public void setDestroyable(boolean[] destroyable)
	{
		this.destroyable = destroyable;
	}

	public Color[] getColor()
	{
		return color;
	}

	public void setColor(Color[] color)
	{
		this.color = color;
	}

	public boolean[] getDestroyed()
	{
		return destroyed;
	}

	public void setDestroyed(boolean[] destroyed)
	{
		this.destroyed = destroyed;
	}
}